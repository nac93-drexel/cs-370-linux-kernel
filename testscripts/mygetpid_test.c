#include <stdio.h>
  
_syscall0(long, mygetpid)

int main (void)
{
  printf("Getpid: %li\n", getpid());
  printf("Mygetpid: %li\n", mygetpid());
  return 0;
}
