#include <stdio.h>

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }

  long pid = atoi(argv[1]);
  printf("Waiting on process [%i]\n", pid);
  long result = syscall(290, pid);
  if (result == 0)
  {
    printf("Process exited, finished waiting\n");
    return 0;
  }
  else
  {
    printf("Error finding process\n");
    return -1;
  }
}
