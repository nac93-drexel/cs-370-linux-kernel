#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }

  int target = atoi(argv[1]);
  printf("Zombifying process [%i]\n", target);
  if (syscall(289, target) != 0)
  {
    printf("Unable to find process\n");
    return -1;
  }
  else
  {
    printf("Process zombified\n");
    return 0;
  }
}
