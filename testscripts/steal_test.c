#include <stdio.h>

int main(int argc, const char** argv) 
{
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }
  int pid = atoi(argv[1]);
  printf("Elevating process #%i\n", pid);
  if (syscall(286, pid) == -2)
  {
    printf("Failed to find process\n");
    return -1;
  }
  return 0;
}
