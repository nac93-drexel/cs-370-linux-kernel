#include <stdio.h>

int main (void)
{
  printf("Mygetpid: %i\n", syscall(285));
  printf("Steal: %i\n", syscall(286, 8));
  printf("Quad: %i\n", syscall(287, 7));
  printf("Swipe: %i\n", syscall(288, 5, 9));
  printf("Zombify: %i\n", syscall(289, 3));
  printf("Myjoin: %i\n", syscall(290, 22));
  printf("Forcewrite: %i\n", syscall(291, 50));
  return 0;
}
