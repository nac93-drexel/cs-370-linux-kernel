
/*
 *  kernel/mailbox.c
 *
 *  Mailbox syscalls implemented for CS 370
 *
 *  Copyright  (C) 2018  Nicholas Colaprete
 */

#include <linux/mm.h>
#include <linux/module.h>
#include <linux/nmi.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/highmem.h>
#include <linux/smp_lock.h>
#include <asm/mmu_context.h>
#include <linux/interrupt.h>
#include <linux/capability.h>
#include <linux/completion.h>
#include <linux/kernel_stat.h>
#include <linux/debug_locks.h>
#include <linux/security.h>
#include <linux/notifier.h>
#include <linux/profile.h>
#include <linux/freezer.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/delay.h>
#include <linux/smp.h>
#include <linux/threads.h>
#include <linux/timer.h>
#include <linux/rcupdate.h>
#include <linux/cpu.h>
#include <linux/cpuset.h>
#include <linux/percpu.h>
#include <linux/kthread.h>
#include <linux/seq_file.h>
#include <linux/syscalls.h>
#include <linux/times.h>
#include <linux/tsacct_kern.h>
#include <linux/kprobes.h>
#include <linux/delayacct.h>
#include <linux/reciprocal_div.h>

#include <asm/tlb.h>
#include <asm/unistd.h>

#define PROC_NOT_FOUND 2
#define NO_MAIL 3

bool get_process(pid_t pid, struct task_struct** task)
{
  struct task_struct* p;
  for_each_process(p)
  {
    if (p->pid == pid)
    {
      *task = p;
      return true;
    }
  }
  return false;
}

long __mysend(pid_t pid, int n, char* buf, bool block)
{
  struct task_struct* reciever_task;
  if (get_process(pid, &reciever_task))
  {
    if (!(reciever_task->mailbox.next))
      INIT_LIST_HEAD(&(reciever_task->mailbox));
    struct task_mail *new_mail = kmalloc(sizeof(struct task_mail), GFP_KERNEL);
    new_mail->sender = current->pid;
    new_mail->message = kmalloc(n, GFP_KERNEL);
    copy_from_user(new_mail->message, buf, n);
    list_add(&(new_mail->other_mails), &(reciever_task->mailbox));
    //printk("Mail sent from [%d] to [%d]: \"%s\"\n", current->pid, pid, new_mail->message);

    if (block)
    {
      set_current_state(TASK_UNINTERRUPTIBLE);
      current->time_slice = 0;
      schedule();
    }
    return 0;
  }
  //errno = PROC_NOT_FOUND;
  return -1;
}

// Syscall 292
asmlinkage long sys_mysend(pid_t pid, int n, char* buf)
{
  return __mysend(pid, n, buf, false);
}

// Syscall 294
asmlinkage long sys_mysend_block(pid_t pid, int n, char* buf)
{
  return __mysend(pid, n, buf, true);
}

// Syscall 293
asmlinkage long sys_myrecieve(pid_t pid, int n, char* buf)
{
  struct list_head *list_pos, *list_elem;
  if (!(current->mailbox.next))
    INIT_LIST_HEAD(&(current->mailbox));
  list_for_each_safe(list_pos, list_elem, &(current->mailbox))
  {
    struct task_mail* mail = list_entry(list_pos, struct task_mail, other_mails);
    //printk("|%d| [%d] Mail: [%d]->\"%s\"\n", pid, current->pid, mail->sender, mail->message);
    if (pid == -1 || mail->sender == pid)
    {
      copy_to_user(buf, mail->message, n);
      list_del(list_pos);
      kfree(mail->message);
      kfree(mail);
      
      struct task_struct* sender_task;
      if (get_process(mail->sender, &sender_task) && sender_task->state > 0)
         wake_up_process(sender_task); 

      return 0;
    }
  }
  //errno = NO_MAIL;
  return -1;
}
