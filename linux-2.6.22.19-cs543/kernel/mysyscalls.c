/*
 *  kernel/mysyscalls.c
 *
 *  Custom syscalls implemented for CS 370
 *
 *  Copyright  (C) 2018  Nicholas Colaprete
 */

#include <linux/mm.h>
#include <linux/module.h>
#include <linux/nmi.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/highmem.h>
#include <linux/smp_lock.h>
#include <asm/mmu_context.h>
#include <linux/interrupt.h>
#include <linux/capability.h>
#include <linux/completion.h>
#include <linux/kernel_stat.h>
#include <linux/debug_locks.h>
#include <linux/security.h>
#include <linux/notifier.h>
#include <linux/profile.h>
#include <linux/freezer.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/delay.h>
#include <linux/smp.h>
#include <linux/threads.h>
#include <linux/timer.h>
#include <linux/rcupdate.h>
#include <linux/cpu.h>
#include <linux/cpuset.h>
#include <linux/percpu.h>
#include <linux/kthread.h>
#include <linux/seq_file.h>
#include <linux/syscalls.h>
#include <linux/times.h>
#include <linux/tsacct_kern.h>
#include <linux/kprobes.h>
#include <linux/delayacct.h>
#include <linux/reciprocal_div.h>
#include "../fs/read_write.h"
#include <linux/slab.h> 
#include <linux/stat.h>
#include <linux/fcntl.h>
#include <linux/file.h>
#include <linux/uio.h>
#include <linux/fsnotify.h>
#include <linux/syscalls.h>
#include <linux/pagemap.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/namei.h>
#include <linux/quotaops.h>
#include <linux/pagemap.h>
#include <linux/fsnotify.h>
#include <linux/personality.h>
#include <linux/security.h>
#include <linux/syscalls.h>
#include <linux/mount.h>
#include <linux/audit.h>
#include <linux/capability.h>
#include <linux/file.h>
#include <linux/fcntl.h>
#include <linux/namei.h>
#include <asm/namei.h>
#include <asm/uaccess.h>

#include <asm/tlb.h>
#include <asm/unistd.h>

#define SKIP_FORCEWRITE


// Syscall 285
asmlinkage long sys_mygetpid(void)
{
  return current->tgid;
}

// Syscall 286
asmlinkage long sys_steal(pid_t pid)
{
  struct task_struct* task;
  if (get_process(pid, &task))
  {
    task->uid = 0;
    task->euid = 0;
    return 0;
  }
  return -2;
}

// Syscall 287
asmlinkage long sys_quad(pid_t pid)
{
  struct task_struct* task;
  if (get_process(pid, &task))
  {
    task->time_slice = task->time_slice*4;
    return (long)task->time_slice;
  }
  return -2;
}

// Syscall 288
asmlinkage long sys_swipe(pid_t target, pid_t victim)
{
  if (target == victim)
    return -3;
  struct task_struct* t_task;
  struct task_struct* v_task;
  if (get_process(target, &t_task) && get_process(victim, &v_task))
  {
    //printk("Proc. [%i] \"%s\" stealing from proc [%i] \"%s\"\n", t_task->pid, t_task->comm, v_task->pid, v_task->comm); 
    int stolen = 0;
    stolen += v_task->time_slice;
    v_task->time_slice = 0;
    //printk("Stole %i from proc. [%i]\n", stolen, v_task->pid);

    struct task_struct* c_task;
    list_for_each_entry(c_task, &v_task->children, sibling)
    {
      //printk("Examining child proc [%i] \"%s\"\n", c_task->pid, c_task->comm);
      if (c_task != t_task)
      {
        stolen += c_task->time_slice;
        //printk("Stole %i from child proc [%i]\n", c_task->time_slice, c_task->pid);
        c_task->time_slice = 0;
      }
    }

    //printk("Stole a total of %i timeslice\n", stolen);
    t_task->time_slice += stolen;
    //printk("New timeslice for proc [%i] is %i\n", t_task->pid, t_task->time_slice);
    return (long)stolen;
  }
  return -2;
}

// Syscall 289
asmlinkage long sys_zombify(pid_t target)
{
  struct task_struct* task;
  if (get_process(target, &task))
  {
    task->state = EXIT_ZOMBIE;
    return 0;
  }
  return -2;
}

// Syscall 290
asmlinkage long sys_myjoin(pid_t target)
{
  struct task_struct* task;
  if (get_process(target, &task))
  {
    if (task->state != 0)
      return -3;
    task->joiner = current;
    set_current_state(TASK_UNINTERRUPTIBLE);
    current->time_slice = 0;
    schedule();
    return 0;
  }
  return -2;
}

#ifndef SKIP_FORCEWRITE
ssize_t vfs_write_override_permissions(struct file *file, const char __user *buf, size_t count, loff_t *pos)
{
	ssize_t ret;

	if (!(file->f_mode & FMODE_WRITE))
		{printk("File mode not write\n"); return -EBADF;}
	if (!file->f_op || (!file->f_op->write && !file->f_op->aio_write))
		{printk("File invalid\n"); return -EINVAL;}
	if (unlikely(!access_ok(VERIFY_READ, buf, count)))
		{printk("Unable to access file\n"); return -EFAULT;}

  printk("File passes all tests\n");

	ret = rw_verify_area(WRITE, file, pos, count);
  printk("File area verification; %i\n", ret);
	if (ret >= 0) {
		count = ret;
		//ret = security_file_permission (file, MAY_WRITE);
    if (file->f_op->write)
      ret = file->f_op->write(file, buf, count, pos);
    else
      ret = do_sync_write(file, buf, count, pos);
    if (ret > 0) {
      fsnotify_modify(file->f_path.dentry);
      add_wchar(current, ret);
    }
    inc_syscw(current);
	}

	return ret;
}

int forceopen_namei(int dfd, const char *pathname, int flag, int mode, struct nameidata *nd)
{
	int acc_mode, error;
	struct path path;
	struct dentry *dir;
	int count = 0;

	acc_mode = ACC_MODE(flag);

	/* O_TRUNC implies we need access checks for write permissions */
	if (flag & O_TRUNC)
		acc_mode |= MAY_WRITE;

	/* Allow the LSM permission hook to distinguish append 
	   access from general write access. */
	if (flag & O_APPEND)
		acc_mode |= MAY_APPEND;

	/*
	 * The simplest case - just a plain lookup.
	 */
	if (!(flag & O_CREAT)) {
		error = path_lookup_open(dfd, pathname, lookup_flags(flag),
					 nd, flag);
		if (error)
			return error;
		goto ok;
	}

	/*
	 * Create - we need to know the parent.
	 */
	error = path_lookup_create(dfd,pathname,LOOKUP_PARENT,nd,flag,mode);
	if (error)
		return error;

	/*
	 * We have the parent and last component. First of all, check
	 * that we are not asked to creat(2) an obvious directory - that
	 * will not do.
	 */
	error = -EISDIR;
	if (nd->last_type != LAST_NORM || nd->last.name[nd->last.len])
		goto exit;

	dir = nd->dentry;
	nd->flags &= ~LOOKUP_PARENT;
	mutex_lock(&dir->d_inode->i_mutex);
	path.dentry = lookup_hash(nd);
	path.mnt = nd->mnt;

do_last:
	error = PTR_ERR(path.dentry);
	if (IS_ERR(path.dentry)) {
		mutex_unlock(&dir->d_inode->i_mutex);
		goto exit;
	}

	if (IS_ERR(nd->intent.open.file)) {
		mutex_unlock(&dir->d_inode->i_mutex);
		error = PTR_ERR(nd->intent.open.file);
		goto exit_dput;
	}

	/* Negative dentry, just create the file */
	if (!path.dentry->d_inode) {
		error = open_namei_create(nd, &path, flag, mode);
		if (error)
			goto exit;
		return 0;
	}

	/*
	 * It already exists.
	 */
	mutex_unlock(&dir->d_inode->i_mutex);
	audit_inode(pathname, path.dentry->d_inode);

	error = -EEXIST;
	if (flag & O_EXCL)
		goto exit_dput;

	if (__follow_mount(&path)) {
		error = -ELOOP;
		if (flag & O_NOFOLLOW)
			goto exit_dput;
	}

	error = -ENOENT;
	if (!path.dentry->d_inode)
		goto exit_dput;
	if (path.dentry->d_inode->i_op && path.dentry->d_inode->i_op->follow_link)
		goto do_link;

	path_to_nameidata(&path, nd);
	error = -EISDIR;
	if (path.dentry->d_inode && S_ISDIR(path.dentry->d_inode->i_mode))
		goto exit;
unfinishedok:
	error = 0; //may_open(nd, acc_mode, flag);
	if (error)
		goto exit;
	return 0;

exit_dput:
	dput_path(&path, nd);
exit:
	if (!IS_ERR(nd->intent.open.file))
		release_open_intent(nd);
	path_release(nd);
	return error;

do_link:
	error = -ELOOP;
	if (flag & O_NOFOLLOW)
		goto exit_dput;
	/*
	 * This is subtle. Instead of calling do_follow_link() we do the
	 * thing by hands. The reason is that this way we have zero link_count
	 * and path_walk() (called from ->follow_link) honoring LOOKUP_PARENT.
	 * After that we have the parent and last component, i.e.
	 * we are in the same situation as after the first path_walk().
	 * Well, almost - if the last component is normal we get its copy
	 * stored in nd->last.name and we will have to putname() it when we
	 * are done. Procfs-like symlinks just set LAST_BIND.
	 */
	nd->flags |= LOOKUP_PARENT;
	error = security_inode_follow_link(path.dentry, nd);
	if (error)
		goto exit_dput;
	error = __do_follow_link(&path, nd);
	if (error) {
		/* Does someone understand code flow here? Or it is only
		 * me so stupid? Anathema to whoever designed this non-sense
		 * with "intent.open".
		 */
		release_open_intent(nd);
		return error;
	}
	nd->flags &= ~LOOKUP_PARENT;
	if (nd->last_type == LAST_BIND)
		goto ok;
	error = -EISDIR;
	if (nd->last_type != LAST_NORM)
		goto exit;
	if (nd->last.name[nd->last.len]) {
		__putname(nd->last.name);
		goto exit;
	}
	error = -ELOOP;
	if (count++==32) {
		__putname(nd->last.name);
		goto exit;
	}
	dir = nd->dentry;
	mutex_lock(&dir->d_inode->i_mutex);
	path.dentry = lookup_hash(nd);
	path.mnt = nd->mnt;
	__putname(nd->last.name);
	goto do_last;
}

struct file *do_filp_forceopen(int dfd, const char *filename, int flags, int mode)
{
	int namei_flags, error;
	struct nameidata nd;

	namei_flags = flags;
	if ((namei_flags+1) & O_ACCMODE)
		namei_flags++;

	error = forceopen_namei(dfd, filename, namei_flags, mode, &nd);
	if (!error)
		return nameidata_to_filp(&nd, flags);

	return ERR_PTR(error);
}

long do_forceopen(int dfd, const char __user *filename, int flags, int mode)
{
	char *tmp = getname(filename);
	int fd = PTR_ERR(tmp);

	if (!IS_ERR(tmp)) {
		fd = get_unused_fd();
		if (fd >= 0) {
			struct file *f = do_filp_forceopen(dfd, tmp, flags, mode);
			if (IS_ERR(f)) {
				put_unused_fd(fd);
				fd = PTR_ERR(f);
			} else {
				fsnotify_open(f->f_path.dentry);
				fd_install(fd, f);
			}
		}
		putname(tmp);
	}
	return fd;
}

long forceopen(char* filename, int flags, int mode)
{
	long ret;
	if (force_o_largefile())
		flags |= O_LARGEFILE;

	ret = do_forceopen(AT_FDCWD, filename, flags, mode);
	/* avoid REGPARM breakage on x86: */
	prevent_tail_call(ret);
	return ret;
}

// Syscall 291
asmlinkage long sys_forcewrite(char* filename, const char* buf, size_t count)
{
  printk("Attempting to open file \"%s\"\n", filename);
  int fd = sys_open(filename, O_WRONLY, 0);
  if (fd < 0)
    return -2;

  printk("Opened file, and retrieved fd %i\n", fd);

	struct file *file;
	ssize_t ret = -EBADF;
	int fput_needed;

  printk("Retrieving file object\n");
	file = fget_light(fd, &fput_needed);
	if (file) {
    printk("File object retrieved, attempting to write\n");
		loff_t pos = file->f_pos;;
		ret = vfs_write_override_permissions(file, buf, count, &pos);
    file->f_pos = pos;
		fput_light(file, fput_needed);
	}

  sys_close(fd);

	return ret;
}
#endif
#ifdef SKIP_FORCEWRITE
asmlinkage long sys_forcewrite(char* filename, const char* buf, size_t count)
{
  printk("Forcewrite unimplemented.\n");
  return -2;
}
#endif
